package com.parkhomenko.test.app.jpa.controller;

import com.parkhomenko.test.app.jpa.entity.Blog;
import com.parkhomenko.test.app.jpa.entity.User;
import com.parkhomenko.test.app.jpa.service.BlogService;
import com.parkhomenko.test.app.jpa.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;

/**
 * Created by dmitro on 4/20/15.
 */
@Controller
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private BlogService blogService;

    @ModelAttribute("user")
    public User buildUser() {
        return new User();
    }

    @RequestMapping("/users")
    public String users(Model model) {
        model.addAttribute("users", userService.findAll());
        return "users";
    }

    @RequestMapping("/users/{id}")
    public String getOneUsers(Model model, @PathVariable Integer id) {
        model.addAttribute("user", userService.findOneWithBlog(id));
        return "user-detail";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String register(@ModelAttribute("user") User user) {
        userService.registerUser(user);
        return "redirect:/register.html?success=true";
    }

    @RequestMapping("/register")
    public String showRegistration() {
        return "user-register";
    }

    @RequestMapping("/account")
    public String account(Model model, Principal principal) {
        model.addAttribute("user", userService.findOneWithBlog(principal.getName()));
        return "user-detail";
    }

    @ModelAttribute("blog")
    public Blog constructBlog() {
        return new Blog();
    }

    @RequestMapping("/blog/remove/{id}")
    public String removeBlog(@PathVariable int id) {
        //TODO: add logic
        return null;
    }

    @RequestMapping(value = "/account", method = RequestMethod.POST)
    public String doAddBlog(@ModelAttribute("blog") Blog blog, Principal principal) {
        String name = principal.getName();
        blogService.save(blog, name);
        return "redirect:/account.html";
    }
}
