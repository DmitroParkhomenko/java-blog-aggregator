package com.parkhomenko.test.app.jpa.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by dmitro on 4/20/15.
 */
@Controller
public class LoginController {

    @RequestMapping("/login-form") //it means that it will handle all next: /login.* urls!!!!!
    public ModelAndView login(@RequestParam(value = "error", required = false) String error) {
        ModelAndView model = new ModelAndView();
        model.setViewName("login");

        if (error != null) {
            model.addObject("error", true);
        }

        return model;
    }
}
