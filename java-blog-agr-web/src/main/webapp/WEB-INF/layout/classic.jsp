<%--
  Created by IntelliJ IDEA.
  User: dmitro
  Date: 4/19/15
  Time: 4:28 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<!DOCTYPE html>

<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>

<%@ include file="../layout/taglib.jsp"%>

<html>
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

    <title><tiles:getAsString name="title"/></title>
</head>
<body>

<div class="container">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href='<spring:url value="/index"/>'>JBA</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href='<spring:url value="/index"/>'>Home</a></li>

                    <%--<security:authorize access="hasRole('ROLE_ADMIN')">--%>
                        <li><a href='<spring:url value="/users"/>'>Users</a></li>
                    <%--</security:authorize>--%>

                    <li><a href='<spring:url value="/register"/>'>Registration</a></li>

                    <%--login.html becaue /login is reserved!!!!!!!--%>
                    <security:authorize access="! isAuthenticated()">
                        <li><a href='<spring:url value="/login-form"/>'>Login</a></li>
                    </security:authorize>

                    <security:authorize access="isAuthenticated()">
                        <li><a href='<spring:url value="/logout"/>'>Logout</a></li>
                    </security:authorize>

                    <security:authorize access="isAuthenticated()">
                        <li><a href='<spring:url value="/account"/>'>My account</a></li>
                    </security:authorize>
                </ul>

            </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
    </nav>






    <tiles:insertAttribute name="body"/>

    <br><br>

    <center>
        <tiles:insertAttribute name="footer"/>
    </center>

</div>

</body>
</html>
