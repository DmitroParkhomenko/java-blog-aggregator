import com.parkhomenko.test.app.jpa.entity.User;
import com.parkhomenko.test.app.jpa.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * Created by dmitro on 4/21/15.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class})
public class ServicesTest {

    private static final int TOTAL_THREADS = 50;

    @Autowired
    private UserService userService;

    private class MyRunnable implements Runnable {

        @Override
        public void run() {
            System.out.println("Is worked");

            User user = new User();
            user.setName("name_" + System.currentTimeMillis());
            user.setEmail("email_" + System.currentTimeMillis() + "@gmail.com");
            user.setPassword("password_" + System.currentTimeMillis());

            userService.registerUser(user);
        }
    }

    @Test
    public void load_user_service_test() {
        ExecutorService executor = Executors.newFixedThreadPool(TOTAL_THREADS);

        for (int index = 0; index < TOTAL_THREADS; index++) {
            Runnable worker = new MyRunnable();
            executor.execute(worker);
        }

        executor.shutdown();

        while (!executor.isTerminated()) {}
        System.out.println("\n Finished all threads!!!!!");
    }
}
