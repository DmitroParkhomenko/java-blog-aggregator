import com.parkhomenko.test.app.jpa.service.InitDbService;
import com.parkhomenko.test.app.jpa.service.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Created by dmitro on 4/21/15.
 */
@Configuration
@ImportResource("/persistenceApplicationContext.xml")
@ComponentScan(basePackages = {"com.parkhomenko.test.app.jpa"})
public class TestConfig {

//    @Bean
//    public InitDbService getInitDbService() {
//        return new InitDbService();
//    }
//
//    @Bean
//    public UserService getUserService() {
//        return new UserService();
//    }
}
