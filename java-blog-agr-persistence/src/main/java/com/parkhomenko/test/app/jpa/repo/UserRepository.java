package com.parkhomenko.test.app.jpa.repo;

import com.parkhomenko.test.app.jpa.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by dmitro on 4/20/15.
 */
public interface UserRepository extends JpaRepository<User, Integer> {
    User findByName(String name);
}
