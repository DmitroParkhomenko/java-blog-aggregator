package com.parkhomenko.test.app.jpa.service;

import com.parkhomenko.test.app.jpa.entity.Blog;
import com.parkhomenko.test.app.jpa.entity.User;
import com.parkhomenko.test.app.jpa.repo.BlogRepository;
import com.parkhomenko.test.app.jpa.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by dmitro on 4/21/15.
 */
@Service
@Transactional
public class BlogService {

    @Autowired
    private BlogRepository blogRepository;

    @Autowired
    private UserRepository userRepository;

    public void save(Blog blog, String name) {
        User user = userRepository.findByName(name);
        blog.setUser(user);
        blogRepository.save(blog);;
    }
}
