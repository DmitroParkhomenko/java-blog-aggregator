package com.parkhomenko.test.app.jpa.repo;

import com.parkhomenko.test.app.jpa.entity.Blog;
import com.parkhomenko.test.app.jpa.entity.Item;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by dmitro on 4/20/15.
 */
public interface ItemRepository extends JpaRepository<Item, Integer> {

    List<Item> findByBlog(Blog blog, Pageable pageable);
}
