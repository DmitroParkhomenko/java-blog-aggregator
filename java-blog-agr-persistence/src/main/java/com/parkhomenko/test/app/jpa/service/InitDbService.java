package com.parkhomenko.test.app.jpa.service;

import com.parkhomenko.test.app.jpa.entity.Blog;
import com.parkhomenko.test.app.jpa.entity.Item;
import com.parkhomenko.test.app.jpa.entity.Role;
import com.parkhomenko.test.app.jpa.entity.User;
import com.parkhomenko.test.app.jpa.repo.BlogRepository;
import com.parkhomenko.test.app.jpa.repo.ItemRepository;
import com.parkhomenko.test.app.jpa.repo.RoleRepository;
import com.parkhomenko.test.app.jpa.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by dmitro on 4/20/15.
 */
@Transactional
@Service
public class InitDbService {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BlogRepository blogRepository;

    @Autowired
    private ItemRepository itemRepository;

    @PostConstruct
    public void init() {
        Role roleUser = new Role();
        roleUser.setName("ROLE_USER");
        roleRepository.save(roleUser);


        Role roleAdmin = new Role();
        roleAdmin.setName("ROLE_ADMIN");
        roleRepository.save(roleAdmin);

        User userAdmin = new User();

        userAdmin.setEnabled(true);
        userAdmin.setName("admin");

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encodePassword = encoder.encode("admin");
        userAdmin.setPassword(encodePassword);

        List<Role> rolesList = new ArrayList<>();
        rolesList.add(roleUser);
        rolesList.add(roleAdmin);

        userAdmin.setRoles(rolesList);

        userRepository.save(userAdmin);

        Blog blogJavavids = new Blog();
        blogJavavids.setName("Javavids");
        blogJavavids.setUrl("http://feeds.feedburner.com/javavids?format=xml");
        blogJavavids.setUser(userAdmin);

        blogRepository.save(blogJavavids);

        Item item = new Item();
        item.setBlog(blogJavavids);
        item.setTitle("my first");
        item.setLink("http://www.javavids.com");

        Calendar c1 = Calendar.getInstance();
        c1.add(Calendar.DATE, 20);
        item.setPublishedDate(new Date(c1.getTimeInMillis()));

        itemRepository.save(item);

        Item item2 = new Item();
        item2.setBlog(blogJavavids);
        item2.setTitle("my second");
        item2.setLink("http://www.javavids.com");
        item2.setPublishedDate(new Date());

        itemRepository.save(item2);
    }
}
