package com.parkhomenko.test.app.jpa.service;

import com.parkhomenko.test.app.jpa.entity.Blog;
import com.parkhomenko.test.app.jpa.entity.Item;
import com.parkhomenko.test.app.jpa.entity.Role;
import com.parkhomenko.test.app.jpa.entity.User;
import com.parkhomenko.test.app.jpa.repo.BlogRepository;
import com.parkhomenko.test.app.jpa.repo.ItemRepository;
import com.parkhomenko.test.app.jpa.repo.RoleRepository;
import com.parkhomenko.test.app.jpa.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dmitro on 4/20/15.
 */
@Service
@Transactional
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    BlogRepository blogRepository;

    @Autowired
    ItemRepository itemRepository;

    @Autowired
    RoleRepository roleRepository;

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User findOneWithBlog(Integer id) {
        User user = userRepository.findOne(id);
        List<Blog> blogList = blogRepository.findByUser(user);

        for (Blog blog : blogList) {
            List<Item> itemList = itemRepository.findByBlog(blog, new PageRequest(0, 10, Sort.Direction.ASC, "publishedDate"));
            blog.setItems(itemList);
//          blog.getItems(); //works too but can not control size, ... ect.
        }

        user.setBlogs(blogList);

        return user;
    }

    public void registerUser(User user) {
        user.setEnabled(true);

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encodedPassword = encoder.encode(user.getPassword());
        user.setPassword(encodedPassword);

        List<Role> rolesList = new ArrayList<>();
        rolesList.add(roleRepository.findByName("ROLE_USER"));
        user.setRoles(rolesList);

        userRepository.save(user);
    }

    public User findOneWithBlog(String name) {
        User user = userRepository.findByName(name);
        return findOneWithBlog(user.getId());
    }
}
