package com.parkhomenko.test.app.jpa.repo;

import com.parkhomenko.test.app.jpa.entity.Blog;
import com.parkhomenko.test.app.jpa.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by dmitro on 4/20/15.
 */

public interface BlogRepository extends JpaRepository<Blog, Integer>  {

    List<Blog> findByUser(User user);
}
