package com.parkhomenko.test.app.jpa.repo;

import com.parkhomenko.test.app.jpa.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by dmitro on 4/20/15.
 */
public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findByName(String name);
}
